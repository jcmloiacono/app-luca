-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 12, 2021 at 06:15 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db-tomasello`
--
CREATE DATABASE IF NOT EXISTS `db-tomasello` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db-tomasello`;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Linea Convenzionale', 'Linea convenzionale senza glutine'),
(2, 'Linea Biologica', 'Linea Bio senza glutine');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id` int(4) NOT NULL,
  `id_agent` int(4) NOT NULL,
  `name` char(50) DEFAULT NULL,
  `lastname` char(50) DEFAULT NULL,
  `name_company` char(50) DEFAULT NULL,
  `p_iva` int(50) DEFAULT NULL,
  `address` char(255) DEFAULT NULL,
  `cap` int(20) DEFAULT NULL,
  `city` char(50) DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `email` char(255) DEFAULT NULL,
  `id_list_price` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `list_price`
--

CREATE TABLE `list_price` (
  `id` int(4) NOT NULL,
  `price20` int(4) DEFAULT NULL,
  `price30` int(4) DEFAULT NULL,
  `price40` int(4) DEFAULT NULL,
  `price` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orden`
--

CREATE TABLE `orden` (
  `id` tinyint(4) NOT NULL,
  `f_ordine` date DEFAULT NULL,
  `id_cliente` int(4) DEFAULT NULL,
  `id_details` int(4) DEFAULT NULL,
  `qta` int(50) DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(4) NOT NULL,
  `id_product` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `qta` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `code` int(4) UNSIGNED ZEROFILL NOT NULL,
  `name` char(50) DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `value` float DEFAULT NULL,
  `id_category` tinyint(4) DEFAULT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`code`, `name`, `stock`, `value`, `id_category`, `image`) VALUES
(0001, 'Crostatina Albicocca 180g - 7 Unit', 200, 20.47, 1, '/static/images/1.jpg'),
(0002, 'Crostatina Arancia 180g - 7 Unit', 200, 20.93, 1, '/static/images/2.jpg'),
(0003, 'Crostatina Frutti Bosco 180g - 7 Unit', 200, 20.47, 1, '/static/images/3.jpg'),
(0004, 'Crostatina Limone 180g - 7 Unit', 200, 20.93, 1, '/static/images/4.jpg'),
(0005, 'Crostatina Mela 180g - 7 Unit', 200, 20.47, 1, '/static/images/5.jpg'),
(0006, 'Crostatina Nocciola 180g - 7 Unit', 200, 20.47, 1, '/static/images/6.jpg'),
(0007, 'Cupcake Arancia 180g - 8 Unit', 200, 23.4, 1, '/static/images/7.jpg'),
(0008, 'Cupcake Cacao 180g - 8 Unit', 200, 23.4, 1, '/static/images/8.jpg'),
(0009, 'Cupcake Fragola 180g - 8 Unit', 200, 23.4, 1, '/static/images/9.jpg'),
(0010, 'Cupcake Limone 180g - 8 Unit', 200, 23.4, 1, '/static/images/10.jpg'),
(0011, 'Tornado 140g - 12 Unit', 200, 36.66, 1, '/static/images/11.jpg'),
(0012, 'Rolls 140g - 12 Unit', 200, 36.66, 1, '/static/images/12.jpg'),
(0013, 'Biscotti alla Mandorla 150g - 8 Unit', 200, 28.6, 2, '/static/images/13.jpg'),
(0014, 'Mix per Dolci 400g - 8 Unit', 200, 28.6, 2, '/static/images/14.jpg'),
(0015, 'Mix per Pizza 400g - 8 Unit', 200, 28.6, 2, '/static/images/15.jpg'),
(0016, 'Mix per Frolla 400g - 8 Unit', 200, 28.6, 2, '/static/images/16.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `utente`
--

CREATE TABLE `utente` (
  `id` int(4) NOT NULL,
  `name` char(255) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  `email` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utente`
--

INSERT INTO `utente` (`id`, `name`, `password`, `email`) VALUES
(1, 'jean', 'jean', 'jean'),
(2, 'jean2', '$2b$12$OX6Pi1ySprD1D7Ed1B3oo.pmb1hAF/S2t8lBFlswelC8U565b04AK', 'jean2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_list_price` (`id_list_price`),
  ADD KEY `id_agent` (`id_agent`);

--
-- Indexes for table `list_price`
--
ALTER TABLE `list_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orden`
--
ALTER TABLE `orden`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_cliente` (`id_cliente`),
  ADD KEY `relation_details_order` (`id_details`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_product` (`id_product`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`code`),
  ADD KEY `id_category` (`id_category`);

--
-- Indexes for table `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`id_list_price`) REFERENCES `list_price` (`id`),
  ADD CONSTRAINT `cliente_ibfk_3` FOREIGN KEY (`id_agent`) REFERENCES `utente` (`id`);

--
-- Constraints for table `list_price`
--
ALTER TABLE `list_price`
  ADD CONSTRAINT `list_price_ibfk_1` FOREIGN KEY (`id`) REFERENCES `cliente` (`id_list_price`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orden`
--
ALTER TABLE `orden`
  ADD CONSTRAINT `relation_client` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `relation_details_order` FOREIGN KEY (`id_details`) REFERENCES `order_details` (`id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `products` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
