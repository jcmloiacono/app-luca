

CREATE TABLE IF NOT EXISTS products(
    code INT(4) UNSIGNED ZEROFILL NOT NULL,
    name CHAR(50),
    stock INT NOT NULL,
    value FLOAT,
    id_category TINYINT NULL,
    PRIMARY KEY(code)
);

CREATE TABLE IF NOT EXISTS categories(
    id TINYINT NOT NULL,
    name CHAR(40) NOT NULL,
    description VARCHAR(200),
    PRIMARY KEY(id)
);

AFTER TABLE products ADD FOREIGN KEY(id_category) REFERENCES categories(id);

CREATE TABLE IF NOT EXISTS utente(
    id TINYINT NOT NULL,
    nome CHAR(50),
    cognome CHAR(50),
    nickname CHAR(50),
    password CHAR(255),
    email CHAR(255),
    PRIMARY KEY(id)

);

CREATE TABLE IF NOT EXISTS cliente(
    id TINYINT NOT NULL,
    id_agent TINYINT NULL,
    name CHAR(50),
    lastname CHAR(50),
    name_company CHAR(50),
    p_iva INT(50),
    address CHAR(255),
    cap INT(20),
    city CHAR(50),
    phone INT(20),
    email CHAR(255),
    id_discount  TINYINT NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS orden(
    id TINYINT NOT NULL, AUTO_INCREMENT,
    date DATE(50),
    id_cliente INT(50),
    id_agent INT(50),
    id_product INT(50),
    qta INT(50),
    iva FLOAT(50),
    total FLOAT(50),
    note TEXT(255),
    PRIMARY KEY(id)
);

