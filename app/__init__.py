from flask import Flask
from app.routes.route import route

app = Flask(__name__)


#Declaracion del Secret Key para poder usar message flashings
app.config.from_mapping(
    SECRET_KEY = 'developmentPassione'
)
#User
app.add_url_rule(route["login_route"], view_func=route["login_controller"])
app.add_url_rule(route["register_route"], view_func=route["register_controller"])
app.add_url_rule(route["logout_route"], view_func=route["logout_controller"])
#HomePage
app.add_url_rule(route["home_route"], view_func=route["home_controller"])
app.add_url_rule(route["customer_route"], view_func=route["customer_controller"])
# Rutas de la aplicacion a productos
app.add_url_rule(route["add_route"], view_func=route["add_controller"])
app.add_url_rule(route["delete_route"], view_func=route["delete_controller"])
app.add_url_rule(route["update_route"], view_func=route["update_controller"])

#Ruta del error 404
app.register_error_handler(route["not_found_route"], route["not_found_controller"])

#Rutas de la aplicacion a categorias
app.add_url_rule(route["categories_route"], view_func=route["categories_controller"])

#Ruta para Crear Orden
app.add_url_rule(route["order_route"], view_func=route["order_controller"])

#Ruta para Exportar Orden
app.add_url_rule(route["create_xml_route"], view_func=route["create_xml_controller"])

#Ruta para importar archivo csv productos
app.add_url_rule(route["upload_products_route"], view_func=route["csv_products_controller"])

#Ruta para importar archivo csv Clientes
app.add_url_rule(route["upload_customers_route"], view_func=route["csv_customers_controller"])
