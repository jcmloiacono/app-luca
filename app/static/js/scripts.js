
function total_product(qtaid, valqta) {
    /* funcion para hacer el calculo del sub-total de cada producto*/ 
    /* el calculo se realiza de la siguiente forma: (precio scontato * Cantidad) - porcentaje extra sconto + IVA */

    let total = 0;
    let discount = 0;
    let extra_discount = 0;
    /* almacena precio unitario */
    let priceunit = 0;
    let idprice = 'price_'.concat(qtaid); /* ID del precio del producto (price_1..n) */
    let iddiscount = 'price_discont_'.concat(qtaid); /*  ID del precio descontado 0 (price_discont_qta_1..n) */
    let iddiscount1 = 'price_discont1_'.concat(qtaid); /*  ID del precio descontado 1 (price_discont_qta_1..n) */
    let iddiscount2 = 'price_discont2_'.concat(qtaid); /*  ID del precio descontado 2 (price_discont_qta_1..n) */
    let iddiscount3 = 'price_discont3_'.concat(qtaid); /*  ID del precio descontado 3 (price_discont_qta_1..n) */
    let idextradiscount = 'extra_discont_'.concat(qtaid); /*  ID del extra descuento  (extra_discont_qta_1..n) */
    let id = qtaid.replace('qta_', "")
    let iva = 'iva'.concat(id); /*  ID del iva  (iva1..n) */

    /* verifica de que lista de precios debe tomar el precio */
    if (document.getElementById('list1_1').style.display == "table-row" ){
        discount = parseFloat(document.getElementById(iddiscount1).value)
        
    }
    else if (document.getElementById('list2_1').style.display == "table-row"){
        discount = parseFloat(document.getElementById(iddiscount2).value)
        
    }
    else if (document.getElementById('list3_1').style.display == "table-row"){
        discount = parseFloat(document.getElementById(iddiscount3).value)
        
    }
    else{
        discount = parseFloat(document.getElementById(iddiscount).value)
        
    }

    total_without_discount = discount * valqta
    extra_discount = total_without_discount * parseInt(document.getElementById(idextradiscount).value)/100;

    if (valqta != 0){
        total = parseFloat((discount * valqta - (extra_discount)));
        
    }
    
    calculo_iva = total * parseInt(document.getElementById(iva).value) / 100;

    document.getElementById('sptotal_' + qtaid).value = total.toFixed(2);
    document.getElementById('iva_monto_'+id).value = calculo_iva.toFixed(2);

    subTotalOrder()
    
    
}

function infoCustomer(value_customer){
    /* Rellena campos "nombre, cod-Cliente, nombre contac, direccion, telefono, email, lista precio y descuento" al seleccionar el cliente*/
    /* Los datos que manda el HTML son {{i.0}},{{i.4}},{{i.11}},{{i.12}},{{i.15}},{{i.17}},{{i.18}} */

    qproduct = parseInt(document.getElementById("quantity_product").innerHTML);
    value_customer = value_customer.split(';')
    
    /* Sacando los datos separados de la lista */
    let customer_id = value_customer[0]; //Id del cliente {{i.0}}
    let customer_address = value_customer[1]; // Direccion {{i.4}}
    let customer_name = value_customer[2]; // Nombre del contacto cliente {{i.11}}
    let customer_phone = value_customer[3].replace(' ', ''); // Telefono {{i.12}}
    let customer_email = value_customer[4]; // Email {{i.15}}
    let customer_discount = value_customer[5]; //Descuento {{i.17}}
    let customer_l_price = value_customer[6]; //Lista del precio {{i.18}}
    
    
    /* Limpieza de los campos cuando cambia de cliente */
    for (i = 1; i<(qproduct) ; i++){
        document.getElementById("qta_"+i).value = 0;
        document.getElementById("sptotal_qta_"+i).value = 0;
        document.getElementById('iva_monto_'+i).value = 0;
    }
    
    document.getElementById("t_order").value = 0;
    document.getElementById('sub_total_order').value = 0;
    document.getElementById('total_iva').value = 0;
    
    
    if (typeof(customer_l_price) == 'undefined') {
        document.getElementById('id_cliente').value = "";
        document.getElementById('nome').value = "";
        document.getElementById('address').value = "";
        document.getElementById('phone').value = "";
        document.getElementById('email').value = "";
        
        customer_l_price = 0;
        customer_discount = 0;
        
    }else{
        document.getElementById('id_cliente').value = customer_id;
        document.getElementById('nome').value = customer_name;
        document.getElementById('address').value = customer_address;
        document.getElementById('phone').value = customer_phone;
        document.getElementById('email').value = customer_email;
        
    }

    discontCustomer(customer_l_price, customer_discount);
} 

function discontCustomer(customer_l_price, customer_discount){
    /* Funcion que cambiara el estilo css de la lista de precios mostrada en la pagina segun la lista de precios correspondiente */
    /* adicionalmente rellena el campo de extra exconto con el porcentaje correspondiente */

    qproduct = parseInt(document.getElementById("quantity_product").innerHTML); /* Verifica la cantidad de productos actuales */
    

    if (customer_l_price == 1){
        for (i=1; i<=qproduct-1; i++){
            document.getElementById("list0_"+i).style.display = "none";
        }
        for (x=1; x<=qproduct-1; x++){
            document.getElementById("list1_"+x).style.display = "table-row";
            
        }
        for (y=1; y<=qproduct-1; y++){
            document.getElementById("list2_"+y).style.display = "none";
        }
        for (z=1; z<=qproduct-1; z++){
            document.getElementById("list3_"+z).style.display = "none";
        }
    }/*end if == 1*/

    else if(customer_l_price == 2){
        for (i=1; i<=qproduct-1; i++){
            document.getElementById("list0_"+i).style.display = "none";
        }
        for (x=1; x<=qproduct-1; x++){
            document.getElementById("list1_"+x).style.display = "none";
        }
        for (y=1; y<=qproduct-1; y++){
            document.getElementById("list2_"+y).style.display = "table-row";
        }
        for (z=1; z<=qproduct-1; z++){
            document.getElementById("list3_"+z).style.display = "none";
        }
    }/*end if == 2*/

    else if(customer_l_price == 3){
        for (i=1; i<=qproduct-1; i++){
            document.getElementById("list0_"+i).style.display = "none";
        }
        for (x=1; x<=qproduct-1; x++){
            document.getElementById("list1_"+x).style.display = "none";
        }
        for (y=1; y<=qproduct-1; y++){
            document.getElementById("list2_"+y).style.display = "none";
        }
        for (z=1; z<=qproduct-1; z++){
            document.getElementById("list3_"+z).style.display = "table-row";
        }
    }/*end if == 3*/

    else {
        for (i=1; i<=qproduct-1; i++){
            document.getElementById("list0_"+i).style.display = "table-row";
        }
        for (f=1; f<=qproduct-1; f++){
            document.getElementById("price_discont_qta_"+f).style.marginTop = "1.1em"
        }
        for (x=1; x<=qproduct-1; x++){
            document.getElementById("list1_"+x).style.display = "none";
        }
        for (y=1; y<=qproduct-1; y++){
            document.getElementById("list2_"+y).style.display = "none";
        }
        for (z=1; z<=qproduct-1; z++){
            document.getElementById("list3_"+z).style.display = "none";
        }
    }/*end else == 4 o 0*/
        
    for (i=1; i<=qproduct-1; i++){
        document.getElementById("extra_discont_qta_"+i).value = customer_discount;
    }
        
}

function subTotalOrder(){
    /* Funcion que realizara la suma de todos los sub-totales que hay en los productos */

    let total = 0;
    let value1 = 0; 
    let total_product= parseInt( document.getElementById("quantity_product").innerHTML );
    

    for (i=1; i<=total_product-1; i++){
        if (typeof(document.getElementById('sptotal_qta_' + i).value) != 'NaN');
        value1 = (parseFloat(document.getElementById('sptotal_qta_' + i).value)); 
        total = value1 + total;
        
       }

    document.getElementById('sub_total_order').value = parseFloat(total).toFixed(2);
    totalIva()
}

function totalIva(){
    /* Funcion que realizara la suma de los impuestos de cada producto, la realice de forma individual preveendo que existan iva distintos */

    let total_iva = 0;
    let total_product= parseInt( document.getElementById("quantity_product").innerHTML );
    
    for (x=1; x<=total_product-1; x++){
        value_iva = parseFloat(document.getElementById('iva_monto_' + x).value);
        total_iva = parseFloat( value_iva + total_iva )
    }

    document.getElementById('total_iva').value = parseFloat(total_iva).toFixed(2);
    totalOrder()
}

function totalOrder(){
    /* funcion que realiza la suma entre sub-Total de la orden y el iva */

    sub_total = document.getElementById('sub_total_order').value;
    total_iva = document.getElementById('total_iva').value;
    
    total_order = parseFloat(sub_total) +  parseFloat(total_iva);

    document.getElementById('t_order').value = parseFloat(total_order).toFixed(2);
}

function funcEnabled(){
    /* Funcion que activa los campos Disabled para que puedan ser enviados en el formulario */

    quantity_product = parseInt(document.getElementById("quantity_product").innerHTML)+1;

    if (confirm('Sei sicuro di inviare i dati per questo ordine?'))
    {   
        document.getElementById("id_ordine").disabled = false;
        document.getElementById("datePicker").disabled = false;
        document.getElementById("agente").disabled = false;
        document.getElementById("id_cliente").disabled = false;
        document.getElementById("sub_total_order").disabled = false;
        document.getElementById("total_iva").disabled = false;
        document.getElementById("t_order").disabled = false;
        
       
        for ( let i = 1; i < quantity_product; i++){
            
            document.getElementById("cod_product_"+i).disabled = false;
            document.getElementById("desc_product_"+i).disabled = false;
            document.getElementById("price_qta_"+i).disabled = false;
            document.getElementById("iva"+i).disabled = false;
            document.getElementById("sptotal_qta_"+i).disabled = false;
            document.getElementById("extra_discont_qta_"+i).disabled = false;
            
            /* verifica de que lista de precios debe tomar el precio */
            if (document.getElementById('list1_1').style.display == "table-row" ){
                document.getElementById("price_discont1_qta_"+i).disabled = false;

            }
            else if (document.getElementById('list2_1').style.display == "table-row"){
                document.getElementById("price_discont2_qta_"+i).disabled = false;

            }
            else if (document.getElementById('list3_1').style.display == "table-row"){
                document.getElementById("price_discont3_qta_"+i).disabled = false;

            }
            else{
                document.getElementById("price_discont_qta_"+i).disabled = false;

            }
            
            
            
            
        }
        document.frm_registro.submit();
        
    }
    else{
        event.preventDefault();
    }
}
