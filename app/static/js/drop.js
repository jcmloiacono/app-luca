
function startedDrop(){
    
    var_drag = document.getElementById("destinyzone");
    
    var_drag.addEventListener("dragenter", function(e){
        e.preventDefault();}, false);

    var_drag.addEventListener("dragover", function(e){
        e.preventDefault();}, false);
    
    var_drag.addEventListener("drop", dropped, false );    
}

function dropped(e){

    e.preventDefault();
    let var_files = e.dataTransfer.files;
    let var_listened = "";

    for (let f=0; f<var_files.length; f++){

        var_listened = var_files[f].name;
    }

    var_drag.innerHTML = var_listened;
}

window.addEventListener("load", startedDrop, false);
