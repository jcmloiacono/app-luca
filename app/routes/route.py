from app.controllers.controller import *
from app.controllers.error import NotFoundController
from app.controllers.product_controller import *
from app.controllers.categories_controller import *
from app.controllers.order_controller import *
from app.controllers.create_xml_controller import *
from app.controllers.customer_controller import *
from app.controllers.csv_products_controller import *
from app.controllers.csv_customer_controller import *

route = {
    #Login
    "login_route": "/", "login_controller": LoginController.as_view("login"),
    "register_route": "/register", "register_controller": RegisterController.as_view("register"),
    "logout_route": "/logout", "logout_controller": LogoutController.as_view("logout"),
    #home
    "home_route": "/home", "home_controller": HomeController.as_view("home"),
    "customer_route": "/addcustomer", "customer_controller": CustomerController.as_view("customer"),
    #Route CRUD products
    "add_route": "/add/product/", "add_controller": AddController.as_view("add"),
    "delete_route": "/delete/product/<int:code>", "delete_controller": DeleteProductController.as_view("delete"),
    "update_route": "/update/product/<int:code>", "update_controller": UpdateProductController.as_view("update"),
    #Route Error
    "not_found_route": 404, "not_found_controller": NotFoundController.as_view("not_found"),
    #Route CRUD category
    "categories_route": "/create/categorie", "categories_controller": CreateCategorieController.as_view("categories"),
    #Route Order
    "order_route": "/add/order", "order_controller": CreateOrderController.as_view("order"),
    #Route Generador XML
    "create_xml_route": "/export", "create_xml_controller": CreateXmlController.as_view("export"), 
    #Route CSV Products
    "upload_products_route": "/upload-products", "csv_products_controller": CsvProductsController.as_view("upload-products"),
    #Route CSV Customer
    "upload_customers_route": "/upload-customers", "csv_customers_controller": CsvCustomerController.as_view("upload-customers")
}