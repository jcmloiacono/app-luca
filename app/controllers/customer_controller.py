'''Modulo para la gestion de los clientes'''

from app.db import mysql
from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session
from flask.helpers import url_for
from flask.views import MethodView



# Clase Registro de clientes
class CustomerController(MethodView):
    def get(self):
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
                
            with mysql.cursor() as cur:
                cur.execute("SELECT * FROM list_price")
                list_price = cur.fetchall()
                cur.execute("SELECT * FROM users")
                utente = cur.fetchall()
                cur.execute("SELECT * FROM types_pay")
                types_pay = cur.fetchall()
            
                return render_template('public/customer.html', list_price = list_price, utente = utente, types_pay = types_pay, user_actual = user_actual.upper())
        else: 
            return render_template('public/login.html')
    
    def post(self):
        cod_fiscale = request.form['cod_fiscale']
        p_iva = request.form['p_iva']
        if p_iva == "":
            p_iva = 0
        name_company = request.form['name_company']
        address = request.form['address']
        cap = request.form['cap'] 
        city = request.form['city'] 
        province = request.form['province'] 
        region = request.form['region'] 
        country = request.form['country'] 
        sdi = request.form['sdi'] 
        name_ref = request.form['name_ref'] 
        phone = request.form['phone'] 
        if phone == "":
           phone = 0
        cell = request.form['cell'] 
        if cell == "":
            cell = 0
        fax = request.form['fax'] 
        if fax == "":
            fax = 0
        email = request.form['email'] 
        email_pec = request.form['email_pec'] 
        discount = request.form['discount'] 
        if discount == "":
            discount = 0
        id_list_pice = request.form['id_list_price'] 
        id_agent = request.form['id_agent'] 
        type_pay = request.form['type_pay'] 
        iban_banck = request.form['iban_banck'] 
        nome_bank = request.form['nome_bank']
        
        with mysql.cursor() as cur: 
            try:
                cur.execute("INSERT INTO customer VALUES(NULL,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                            (cod_fiscale, p_iva, name_company, address, cap, city, province, region, country, sdi, name_ref, phone, cell, fax, email, email_pec, discount, id_list_pice, id_agent, type_pay, iban_banck, nome_bank)) 
                cur.connection.commit()
                flash("Il cliente è stato aggiunto con successo", 'success')
                return redirect(url_for('customer'))
            except Exception as e:
                print(e) 
                flash('Si è verificato un errore, controlla i datti inserito', 'error')
                return redirect(url_for('customer'))