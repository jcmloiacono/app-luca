'''Modulo para la creacion de la orden de compra'''

from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session
from flask.helpers import url_for
from flask.views import MethodView
from datetime import datetime

from app.db import mysql
#from app import *

# CONTROLADOR PARA CREAR ORDEN
class CreateOrderController(MethodView):
       
    def get(self):
        # VERIFICACION DE USUARIO CONECTADO
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
                
            with mysql.cursor()as cur:
                cur.execute("SELECT * FROM products")
                data= cur.fetchall()
                n_product = len(data)
                cur.execute("SELECT * FROM categories")
                categories = cur.fetchall()
                cur.execute("SELECT * FROM customer")
                cliente = cur.fetchall()
                cur.execute("SELECT * FROM users")
                utente = cur.fetchall()
                
                #aqui hago la consulta de la base de datos para ver cual es la ultima orden y sumo 1
                cur.execute("SELECT * FROM orders")
                n_all_order = len(cur.fetchall())+1
                print(n_all_order)
                
                #aqui capturo la fecha actual y se la paso al formulario
                now =  datetime.now()
                timeactual = now.strftime("%d" + '/'+"%m"+ '/'"%Y")
        # ENVIO DEL USUARIO CONECTADO DESDE LA BASE DE DATOS
            for i in utente:
                if user_actual.upper() == i[1].upper():
                    utente_id = i[0]
                    
                
            return render_template('public/ordine.html', n_product = n_product, n_all_order = n_all_order, cliente = cliente, user_actual = user_actual.upper(), data = data, categories = categories, utente_id = utente_id, timeactual = timeactual)
        
        else:
            flash("Area riservata, devi essere registrato per entrare in questa sezione.", 'error')
            return redirect('/')
        
    def post(self):
        f_ordine = request.form['datePicker'].split("/")
        nf_ordine = f_ordine[2] + "/" + f_ordine[1] + "/" + f_ordine[0] #Construye el formato Fecha para pasarlo a la base de datos
        
        id_agente= request.form['agente']
        id_customer = request.form['id_cliente']
        sub_total_order = request.form['sub_total_order']
        total_tax = request.form['total_iva']
        total_ordine = request.form['t_order']
        note = request.form['note']
        
        # LLAMADA A LA BASE DE DATOS PARA CREAR EL NUMERO DE LA ORDEN Y PASAR LOS DATOS
        with mysql.cursor() as cur:
            try:
                cur.execute("INSERT INTO orders VALUES(NULL, %s, %s, %s, %s, %s, %s, %s)",(nf_ordine, id_agente, id_customer, sub_total_order, total_tax, total_ordine, note))
                cur.connection.commit()
                
                # LLAMADA A LA BASE DE DATOS PARA CREAR LOS DETALLES DE LA ORDEN
                cur.execute("SELECT * FROM products")
                n_product = cur.fetchall()
                print ("la cantidad de productos es: ", n_product)
                
                for i in range(1, len(n_product)+1):
                    print("el valor de nproductos= ", len(n_product))
                    print("el Valor de i es: ", i)
                    
                    id_order = request.form['id_ordine']
                    qta = request.form['qta_{}'.format(i)]
                    price_descounted = request.form['price_discont_qta_{}'.format(i)]
                    id_extra_discount = request.form['extra_discont_qta_{}'.format(i)]
                    id_iva = request.form['iva_{}'.format(i)]
                    sub_total = request.form['sptotal_qta_{}'.format(i)]
                    desc_product = request.form['desc_product_{}'.format(i)]
                    
                    # AL RECORRER TODOS LOS PRODUCTOS VERIFICA SI HA INGRESADO CANTIDAD
                    if qta == "0" or qta == "":
                        pass
                    else:
                        cur.execute("INSERT INTO order_details VALUES(NULL,%s,%s,%s,%s,%s,%s,%s)",(id_order, qta, price_descounted, id_extra_discount, id_iva, sub_total, desc_product))
                        cur.connection.commit() 
                
                flash("L'ordine è stato aggiunto con successo", 'success')
            except Exception as e:
                print (e)
                flash("Si è verificato un errore durante l'aggiunta del comando", 'error')
                return redirect(url_for('order'))
            
            return redirect(url_for('order'))
        
            