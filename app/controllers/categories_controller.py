'''Modulo para la creacion de las categorias'''

from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session
from flask.helpers import url_for
from flask.views import MethodView

from app.db import mysql

# CONTROLADOR PARA CREAR CATEGORIA
class CreateCategorieController(MethodView):
    
    def get(self):
        global n_categories
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
            
            with mysql.cursor() as cur:  
                cur. execute("SELECT * FROM categories")
                list_categories = cur.fetchall()
                
                n_categories = len(list_categories)+1   
        
            return render_template("public/categories.html", ncategories = n_categories, list_categories = list_categories, user_actual = user_actual.upper())
        else:
            flash("Area riservata, devi essere registrato per entrare in questa sezione.", 'error')
            return redirect(url_for ('/'))
        
    def post(self):
        name = request.form['name']
        description = request.form['description']
        if description == "":
            description = "Senza descrizione"
        
        with mysql.cursor() as cur:
            try:
                cur.execute("INSERT INTO categories VALUES(%s, %s, %s)",(n_categories, name, description))
                cur.connection.commit()
                flash('Categoria aggiunta con successo', 'success')
                return redirect(url_for('categories'))
            except Exception as e:
                print (e)
                flash("Si è verificato un errore durante l'aggiunta della categoria", 'error')
                return redirect(url_for('categories'))
