'''Modulo para la gestion de los productos Crear/Modificar/Eliminar'''

from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session
from flask.views import MethodView

from app.db import mysql

class AddController(MethodView):
    # Clase que controlara el CRUD  de los productos    
    def get(self):
        # Funcion que recibira la llamada /add_product.html
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
              
            with mysql.cursor()as cur:
                cur.execute("SELECT * FROM products")
                data= cur.fetchall()
                n_product = len(data)+1
                
                cur.execute("SELECT * FROM categories")
                categories = cur.fetchall()
                cur.execute("SELECT * FROM tax")
                iva = cur.fetchall()
                return render_template('public/add_product.html', data=data, categories=categories, iva = iva, n_product = n_product, user_actual = user_actual.upper())
        else:
            flash("Area riservata, devi essere registrato per entrare in questa sezione.", 'error')
            return redirect('/')
    
    def post(self):
        id_product = request.form['id_product']
        code = request.form['code']
        name = request.form['name']
        stock = request.form['stock']
        value = request.form['value']
        id_iva = request.form['id_iva']
        
        cod_bar = request.form['cod_bar']
        if cod_bar == "":
            cod_bar = 0
            
        value1 = request.form['value1'] 
        if value1 == '':
            value1 = 0

        value2 = request.form['value2']
        if value2 == "":
            value2 = 0
        
        value3 = request.form['value3']
        if value3 == "":
            value3 = 0
        
        
        category = request.form['category']
        image = request.files['image']
        filename = code
        image.save("src/static/images/" + str(filename) + ".jpg" )
        route_img = ("/static/images/" + str(filename) + ".jpg" )
        #envio de datos a la BBDD
        with mysql.cursor() as cur:
            try:
                cur.execute("INSERT INTO products VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            (id_product,code, name, stock, value, value1, value2, value3, id_iva, cod_bar, category, route_img))
                cur.connection.commit()
                flash('Il prodotto è stato aggiunto con successo', 'success')
            except Exception as e:
                print (e)
                flash("Si è verificato un errore durante l'aggiunta del prodotto", 'error')
            return redirect(url_for('add'))
            #return render_template('public/add_product.html')
            #TODO AQUI PUEDE ESTAR EL ERROR DEL PRODUCTO CUANDO CARGA
            

# CONTROLADOR PARA ELIMINAR PRODUCTO
class DeleteProductController(MethodView):
    def post(self, code):
        try:
            with mysql.cursor() as cur:
                cur.execute("DELETE FROM products WHERE id=%s",(code))
                cur.connection.commit()
                flash('Il prodotto è stato rimosso con successo', 'success')
        except Exception as e:
            print (e)
            flash('Si è verificato un errore durante la cancellazione del prodotto', 'error')
        return redirect(url_for('add'))
               
# CONTROLADOR PARA ACTUALIZAR PRODUCTO
class UpdateProductController(MethodView):
    def get(self, code):
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
                
            with mysql.cursor() as cur:
                cur.execute("SELECT * FROM products WHERE id = %s", (code))
                product = cur.fetchone()
                cur.execute("SELECT * FROM categories")
                categories = cur.fetchall()
                return render_template("public/update.html", product = product, categories = categories, user_actual = user_actual.upper())
        else: 
            flash("Area riservata, devi essere registrato per entrare in questa sezione.", 'error')
            return redirect('/')
        
    def post(self, code):
        code_int = request.form['code']
        name = request.form['name']
        stock = request.form['stock']
        value = request.form['value']
        category = request.form['category']
        
        image = request.files['image']
        filename = (code)
        if image != "":
            image.save("src/static/images/" + str(filename) + "_mod.jpg" )
            route_img = ("/static/images/" + str(filename) + "_mod.jpg" )
        else: 
            with mysql.cursor() as cur:
                cur.execute("SELECT image from products where id = %s",(code))
                image_act = cur.fetchone()
                route_img = image_act[0]
                
        try:
            with mysql.cursor() as cur:
                cur.execute("UPDATE products SET code_int = %s, name= %s, stock = %s, value = %s, id_category = %s, image = %s WHERE id = %s",(code_int, name, stock, value, category, route_img, code))
                cur.connection.commit()
                flash('Il prodotto è stato aggiornato con successo', 'success')
        except Exception as e :
            print (e)
            flash("Si è verificato un errore durante l'aggiornamento del prodotto", 'error')    
        return redirect(url_for('add'))
        