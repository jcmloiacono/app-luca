'''Modulo para la gestion de los usuarios que usan el sistema'''

from app.db import mysql
from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session

def AccessSecure(user_actual):
   
    with mysql.cursor() as cur:
        cur.execute("SELECT * FROM utente WHERE name = %s", (user_actual))
        access_utente = cur.fetchone()
                
        if access_utente[4] != 1: 
            return ("False")
        