'''Modulo para el envio del archivo documenti.defxml via email'''

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from smtplib import SMTP

from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session

def sendEmail():
    try:
        message = MIMEMultipart('plain')
        message['from']='passionesiciliaweb@gmail.com'
        message['to']='web@passionesicilia.it'
        message['subject']='È stato creato un nuovo ordine' 

        attachmend_add = MIMEBase('application' , 'octect-stream')
        attachmend_add.set_payload(open('../../src/static/docs/documenti.defxml', 'rb').read())
        attachmend_add.add_header('content-Disposition' , 'attachment; filename="documenti.defxml"')
        message.attach(attachmend_add)

        smtp = SMTP('smtp.gmail.com')
        smtp.starttls()
        smtp.login('passionesiciliaweb@gmail.com', 'passione2022')
        smtp.sendmail('web@passionesicilia.it', 'passionesiciliaweb@gmail.com', message.as_string())
        smtp.quit()
        flash("Email inviata con successo", 'success')
        return (render_template('public/export.html'))
    except:
        flash("stiamo avendo problemi con l'invio della posta, si prega di riprovare più tardi.", 'error')
        return (render_template('public/export.html'))
    