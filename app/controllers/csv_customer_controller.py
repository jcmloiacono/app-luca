import csv
import codecs
from dataclasses import replace

from flask import request, render_template, redirect, flash, redirect, url_for, session
from flask.views import MethodView

from app.db import mysql
from app import *

class CsvCustomerController(MethodView):
    
    def post(self): 
        
        file_csv = request.files['file_csv']
        #TODO VERIFICAR DE DONDE VIENE LA SOLICITUD "CLIENTE" O "PRODUCTOS" SE PUEDE HACER CON UN 
        #ENVIO DEL FORMULARIO QUE LO RECIBA Y COMPARE
        
        #TODO ABRIR CONEXION CON BASE DE DATOS PRODUCTOS        
        
        reader = csv.reader(codecs.iterdecode(file_csv, "utf-8")) 
        
        cont = 1
        
        for row in reader:
            #AGREGAR A LA BASE DE DATOS CORRESPONDIENTE LOS DATOS OBTENIDOS POR EL ARCHIVO CSV
            if row[0] != "" and cont >= 2:
                cod_fiscale = row[1]
                print ("codigo fiscal: ", cod_fiscale) #string
                
                p_iva = int(row[2])
                print ("partida Iva: ", p_iva) #integer
               
                name_company = str(row[3])
                print ("Nombre Compania: ", name_company) #string
                
                address = str(row[4])
                print ("direccion: ", address) #string
                
                cap = int(row[5])
                print ("codigo Postal: ", cap) #integer
                
                city = str(row[6])
                print ("Ciudad: ", city) #string
                
                province = str(row[7])
                print ("Provincia: ",  province) #string
                
                region = str(row[8])
                print ("Region: ", region) #string
                
                country = str(row[9])
                print ("Pais: ", country) #string
               
                sdi = str(row[10])
                print ("SDI: ", sdi) #string
                
                name_ref = str(row[12])
                print ("Referido: ", name_ref) #integer
                
                phone = row[13]
                print ("Telefono: ", phone) #integer
                
                cell = row[14]
                print ("Celular: ", cell) #integer
                
                fax = row[15]
                print ("Fax: ", fax) #integer
               
                email = str(row[16])
                print ("Email: ", email) #string
                
                email_pec = str(row[17])
                print ("email-pec: ", email_pec) #string
                
                discount = row[18]
                if discount != "":
                    discount = int(row[18])
                    print ("Descuento: ", discount) #string
                else:
                    discount = 0
                    print ("Descuento: ", discount) #string
                
                id_list_price  = row[19]
                if id_list_price == "Listino 40":
                    id_list_price = 1
                    print ("Id lista precio:", id_list_price) #integer
                elif id_list_price == "LISTINO 35":
                    id_list_price = 2
                    print ("Id lista precio:", id_list_price) #integer
                elif id_list_price == "LISTINO NES":
                    id_list_price = 3
                    print ("Id lista precio:", id_list_price) #integer
                else:
                    id_list_price = 4
                    print ("Id lista precio:", id_list_price) #integer
                
                id_agent  = row[21]
                #TODO debe abrir la base de datos y buscar la relacion entre en nombre y el id
                id_agent = 3
                print ("id Agente: ", id_agent) #integer
                
                id_types_pay   = row[22]
                #TODO debe abrir la base de datos y buscar la relacion entre en nombre del pago y el id
                id_types_pay = 2
                print ("Id Tipo Pagamento: ", id_types_pay) #integer
                
                iban_bank  = str(row[23])
                print ("IBAN: ", iban_bank) #string
                
                name_bank  = str(row[24])
                print ("Nombre del Banco: ", name_bank) #string
                
                
                with mysql.cursor() as cur: 
                    cur.execute("INSERT INTO customer VALUES(NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (
                        cod_fiscale, p_iva, name_company, address, cap, city, province, region, country, sdi, name_ref, phone, cell, fax, email, email_pec, discount, id_list_price, id_agent, id_types_pay, iban_bank, name_bank))
                    cur.connection.commit()
                
                
            cont+=1
        
            
            #TODO AGREGAR LA EXCEPTION QUE ENVIE ERROR CUANDO EL ARCHIVO NO ES COMPATIBLE
        print ("**************** FIN ******************")
        return ("todo ok")
    