import csv
import codecs
from dataclasses import replace

from flask import request, render_template, redirect, flash, redirect, url_for, session
from flask.views import MethodView

from app.db import mysql
#from app import *

class CsvProductsController(MethodView):
    
    def post(self): 
        
        file_csv = request.files['file_csv']
        #TODO VERIFICAR DE DONDE VIENE LA SOLICITUD "CLIENTE" O "PRODUCTOS" SE PUEDE HACER CON UN 
        #ENVIO DEL FORMULARIO QUE LO RECIBA Y COMPARE
        
        #TODO ABRIR CONEXION CON BASE DE DATOS PRODUCTOS        
        reader = csv.reader(codecs.iterdecode(file_csv, "utf-8"))
        
        with mysql.cursor() as cur: 
            cur.execute("SELECT * FROM products")
            next_product = len(cur.fetchall()) 
        
        cont = 1
        
        for row in reader:
            #AGREGAR A LA BASE DE DATOS CORRESPONDIENTE LOS DATOS OBTENIDOS POR EL ARCHIVO CSV
            if row[0] != "" and cont >= 2:
                id_product = next_product
                code_int = int(row[0])
                name = str(row[1])
                stock = int(row[36])
                value = float(row[7].replace("€", ""))
                value1 = float(row[8].replace("€", ""))
                value2 = float(row[9].replace("€", ""))
                value3 = float(row[10].replace("€", ""))
                id_iva = int(row[6])
                if id_iva == 10:
                    id_iva = 1
                else:
                    id_iva = 2
                cod_bar = int(row[16])
                id_category = (row[3])
                if id_category == "PRODOTTO FINITO A MARCHIO":
                    id_category = 1
                else:
                    id_category = 2        
                immagine = str(row[48])
                
                with mysql.cursor() as cur: 
                    cur.execute("INSERT INTO products VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (id_product, code_int, name, stock, value, value1, value2, value3, id_iva, cod_bar, id_category, immagine))
                    cur.connection.commit()
                
            next_product+=1
            cont+=1
        
            
            #TODO AGREGAR LA EXCEPTION QUE ENVIE ERROR CUANDO EL ARCHIVO NO ES COMPATIBLE
        print ("**************** FIN ******************")
        return ("todo ok")
    