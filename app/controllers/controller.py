'''Modulo principal para el home page y gestion de los usuarios del sistema'''

import bcrypt

from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session
#from flask.helpers import url_for
from flask.views import MethodView
from typing_extensions import *
from logging import error

from app.db import mysql
from app import *

#from types import TracebackType
#from datetime import datetime

semilla= bcrypt.gensalt()

#Clase Login
class LoginController(MethodView):
    def get(self):
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
                
            with mysql.cursor()as cur:
                cur.execute("SELECT * FROM users")
                utente = cur.fetchall()
            #Carga Template home.html
            
            return render_template('/public/home.html' , utente = utente, user_actual = user_actual.upper())
            
        else: 
            #Carga Template Login "/"
            return render_template('/public/login.html')
        
    def post(self):
        
        if request.method == 'POST':
            name = request.form['nickname']
            password = request.form['password']
            password_encode = password.encode("utf-8")
            
            with mysql.cursor()as cur: 
                squery = "SELECT name, password, id_typeAccess FROM users WHERE name = %s"
                cur.execute(squery, [name])
                # obtenemos datos
                datauser = cur.fetchone()
                cur.close()
                
                #Verificacion si se obtuvo datos
                if (datauser !=None):
                    password_encriptado_encode = datauser[1].encode("utf-8")
                    #obtiene el password enctriptado encode
                    if (bcrypt.checkpw(password_encode,password_encriptado_encode )):
                        session['nombre'] = datauser[0]
                        if datauser[2] != 1:
                            return redirect(url_for('order'))
                        #redirige al home
                        else:
                            return redirect(url_for('home'))
                    else:
                        flash('Password incorrecto', error)
                        return render_template('public/login.html')                        
                
                
                else:
                    flash("Nome utente o password Invialido", 'error')
                    return render_template('public/login.html')

#Clase Registro de usuarios del sistema
class RegisterController(MethodView):
    def get(self):
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
            
            with mysql.cursor() as cur:
                cur.execute("SELECT * FROM type_access")
                type_access = cur.fetchall()
                return render_template('public/register.html', type_access=type_access, user_actual = user_actual.upper())
        else:
            flash("accesso non consentito", "error")
            return redirect(url_for("home"))
            
    def post(self):
        #TODO CREAR TEMPLATE REGISTRAR 
        
        name = request.form['nickname_registro']
        password = request.form['password_registro']
        email = request.form['email_registro']
        type_access = request.form['type_access']
        commission = request.form['commission']
        
        password_encode = password.encode("utf-8")
        password_encriptado = bcrypt.hashpw(password_encode, semilla)
        
        with mysql.cursor() as cur:
            try:
                cur.execute("INSERT INTO users VALUES(NULL,%s, %s, %s, %s, %s)",(name, password_encriptado,email, type_access, commission))
                cur.connection.commit()
                flash("L'utente è stato aggiunto con successo", 'success')
                # Registro de la session
                session['nombre'] = name
                #session['correo'] = correo
                return redirect(url_for('home'))
                
            except Exception() as e:
                print (e)
                flash('Si è verificato un errore', 'error')
                return render_template('public/register.html')

# Clase Salir del sistema                
class LogoutController(MethodView):
    def get(self):
        session.clear()
        return redirect(url_for('login'))     

# Clase inicio app
class HomeController(MethodView):
    def get(self):
        # Sacando el valor del usuario de la seccion del diccionario
        if 'nombre' in session:
            for n in session.values():
                user_actual = n
            return render_template('public/home.html', user_actual = user_actual.upper())
        else: 
            #Carga Template Login "/"
            return render_template('public/login.html')
        
