import os
import xml.etree.cElementTree as ET

from flask import Flask, request, render_template, redirect, flash, redirect, url_for, session
from flask.helpers import url_for
from flask.views import MethodView

from app.db import mysql
from app.controllers import * 

class CreateXmlController(MethodView):
      # Recibe la llamada http, comprueba el nombre y envia datos del usuario conectado
      def get(self):
            if 'nombre' in session: 
              for n in session.values():
                    user_actual = n
                    return render_template('public/export.html', user_actual = user_actual)
            else: 
              return render_template('/public/login.html')

      def post(self):
            #Recibe la peticion HTTP de la fecha inicial y la fecha final
            initdate = request.form["initdate"].replace("-","")
            enddate = request.form["enddate"].replace("-","")
            
            #### CONSULTA A LA BASE DE DATOS PARA MOSTRAR ORDENES SEGUN LA FECHA ####
            with mysql.cursor() as cur:
                  cur.execute("SELECT * FROM orders WHERE d_order BETWEEN %s AND %s", (initdate, enddate))
                  orders_select = cur.fetchall()
                  cont_orders = str(len(orders_select))
                  
                  # Verifica que existan datos que descargar
                  if len(orders_select) == 0:
                        flash("Nessun dato da mostrare", "error")
                        return redirect(url_for("home"))
                  else:
                        
                        #######CABECERA DEL XML#############
                        xml = ET.Element("xml", version=1.0, encoding="UTF-8" )
                        root = ET.Element("EasyfattDocuments", AppVersion="2", Creator="Passione SiciliaADV", CreatorUrl="http://www.passionesiciliaadv.it", xsi="http://www.w3.org/2001/XMLSchema-instance", noNamespaceSchemaLocation="https://www.danea.it/public/easyfatt-xml.xsd")
                        

                        #######DATOS DE LA EMPRESA#############
                        company= ET.SubElement(root, "Company")
                        ET.SubElement(company, "Name").text = "LUCA TOMASELLO"
                        ET.SubElement(company, "Address").text = "VIA DON ALFIO SIGNORELLO 23"
                        ET.SubElement(company, "Postcode").text = "95032"
                        ET.SubElement(company, "City").text = "BELPASSO"
                        ET.SubElement(company, "Province").text = "CT"
                        ET.SubElement(company, "Country").text = "Italia"
                        ET.SubElement(company, "FiscalCode").text = "TMSLCU94C23C351X"
                        ET.SubElement(company, "VatCode").text = "05594960873"
                        ET.SubElement(company, "Tel").text = "095 6244805"
                        ET.SubElement(company, "Email").text = "info@lucatomasello.it"
                        ET.SubElement(company, "HomePage").text = "lucatomasello.it"
                        
                        #######BUCLE PARA RECORRER LAS ORDENES#############
                        documents = ET.SubElement(root, "Documents")
                        
                        for i in orders_select:
                              id_order = i[0]
                              d_order = i[1]
                              id_user = i[2]
                              id_customer = i[3]
                              sub_total_order = i[4]
                              total_tax = i[5]
                              total = i[6]
                              note = i[7]
                              
                              #### CONSULTAS A LA BASE DE DATOS DE LOS DETALLES DE LA ORDEN ####
                              cur.execute("SELECT * FROM order_details WHERE id_order = %s", (id_order))
                              details_orders_select = cur.fetchall()
                              
                              #######BUCLE PARA RECORRER LOS DETALLES DE LA ORDEN#############
                              for d in  details_orders_select:
                                    id_details = d[0]
                                    id_order_details = d[1]
                                    qta_details = d[2]
                                    price_unit_details = d[3]
                                    id_discount_details = d[4]
                                    iva_details = d[5]
                                    
                                    
                              #### CONSULTAS A LA BASE DE DATOS DEL CLIENTE ####      
                              cur.execute("SELECT * FROM customer WHERE id = %s", (id_customer))
                              details_customer_select = cur.fetchall()
                              
                              details_customer_id = details_customer_select[0][0]
                              details_customer_fiscale = str(details_customer_select[0][1])
                              details_customer_p_iva = details_customer_select[0][2]
                              details_customer_name_comp = details_customer_select[0][3]
                              details_customer_addess = details_customer_select[0][4]
                              details_customer_cap = str(details_customer_select[0][5])
                              details_customer_city = details_customer_select[0][6]
                              details_customer_province = details_customer_select[0][7]
                              details_customer_region = details_customer_select[0][8]
                              details_customer_country = details_customer_select[0][9]
                              details_customer_sdi = details_customer_select[0][10]
                              details_customer_nameref = details_customer_select[0][11]
                              details_customer_phone = str(details_customer_select[0][12]).replace(" ","")
                              details_customer_cell = str(details_customer_select[0][13]).replace(" ","")
                              details_customer_fax = str(details_customer_select[0][14]).replace(" ","")
                              details_customer_email = details_customer_select[0][15]
                              details_customer_emailpec = details_customer_select[0][16]
                              details_customer_discount = details_customer_select[0][17]
                              details_customer_listprice = str(details_customer_select[0][18])
                              details_customer_agent = str(details_customer_select[0][19])
                              details_customer_typepay = str(details_customer_select[0][20])
                              details_customer_iban = str(details_customer_select[0][21])
                              details_customer_namebank = details_customer_select[0][22]
                                                            
                              #### CONSULTAS A LA BASE DE DATOS DEL VENDEDOR - USUARIO ####      
                              cur.execute("SELECT * FROM users WHERE id = %s", (id_user))
                              details_user_select = cur.fetchall()
                              
                              #####INFORMACION SEGUN EL NUMERO DE ORDEN#######
                              
                              doc = ET.SubElement(documents, "Document")
                              ET.SubElement(doc, "CustomerCode").text = str(details_customer_id) #Codigo Cliente
                              ET.SubElement(doc, "CustomerWebLogin").text = "" #vacio
                              ET.SubElement(doc, "CustomerName").text = details_customer_name_comp #Nombre Azienda
                              ET.SubElement(doc, "CustomerAddress").text = details_customer_addess #direcciom
                              ET.SubElement(doc, "CustomerPostcode").text = details_customer_cap #CAP
                              ET.SubElement(doc, "CustomerCity").text = details_customer_city #Ciudad
                              ET.SubElement(doc, "CustomerProvince").text = details_customer_province #Provincia
                              ET.SubElement(doc, "CustomerCountry").text = details_customer_country #Pais
                              ET.SubElement(doc, "CustomerFiscalCode").text = details_customer_fiscale #Codigo Fiscal
                              ET.SubElement(doc, "CustomerVatCode").text = "" #Codigo Fiscal VER EN LA DOCUMENTACION QUE DATO TRAE
                              ET.SubElement(doc, "CustomerReference").text = details_customer_nameref #nombre del titular de la empresa
                              ET.SubElement(doc, "CustomerTel").text= details_customer_phone #telefono
                              ET.SubElement(doc, "CustomerEmail").text = details_customer_email #email
                              ET.SubElement(doc, "CustomerPec").text = details_customer_emailpec #email PEC
                              ET.SubElement(doc, "CustomerEInvoiceDestCode").text = "" #VER EN LA DOCUMENTACION QUE DATO TRAE
                              ET.SubElement(doc, "DocumentType").text = "C" #tipo de documento "Orden de compra"
                              ET.SubElement(doc, "Date").text = str(d_order.strftime("%Y-"+"%m-"+"%d")) #fecha
                              ET.SubElement(doc, "Number").text = details_customer_phone #Telefono 
                              ET.SubElement(doc, "Numbering").text = details_customer_cell #celular 
                              ET.SubElement(doc, "CostDescription").text = "" #vacio
                              ET.SubElement(doc, "CostVatCode").text = "" #vacio
                              ET.SubElement(doc, "CostAmount").text = "" #vacio
                              ET.SubElement(doc, "ContribDescription").text = "" #vacio
                              ET.SubElement(doc, "ContribPerc").text = "" #vacio
                              ET.SubElement(doc, "ContribSubjectToWithholdingTax").text = "" #vacio
                              ET.SubElement(doc, "ContribAmount").text = "" #vacio
                              ET.SubElement(doc, "ContribVatCode").text = "" #vacio
                              ET.SubElement(doc, "TotalWithoutTax").text = str(total) #TOTAL SIN IVA
                              ET.SubElement(doc, "VatAmount").text = str(total_tax) #VALOR DEL IMPUESTO EN EUROS
                              ET.SubElement(doc, "WithholdingTaxAmount").text = "" #vacio
                              ET.SubElement(doc, "WithholdingTaxAmountB").text = "" #vacio
                              ET.SubElement(doc, "WithholdingTaxNameB").text = "" #vacio
                              ET.SubElement(doc, "Total").text = str(sub_total_order) #TOTAL CON IVA
                              ET.SubElement(doc, "PriceList").text = details_customer_listprice #tipo de lista de precios
                              ET.SubElement(doc, "PricesIncludeVat").text = "false" #false Verificar que tipo de datos es
                              ET.SubElement(doc, "TotalSubjectToWithholdingTax").text = "" #verificar tipo de datos
                              ET.SubElement(doc, "WithholdingTaxPerc").text = "" #verificar tipo de datos
                              ET.SubElement(doc, "WithholdingTaxPerc2").text = "" #verificar tipo de datos
                              ET.SubElement(doc, "PaymentName").text = details_customer_typepay #Credito para el pago
                              ET.SubElement(doc, "PaymentBank").text = details_customer_iban #IBAN 
                              ET.SubElement(doc, "PaymentAdvanceAmount").text = "" #vacio
                              ET.SubElement(doc, "Carrier").text = "" #TIPO DE ENVIO
                              ET.SubElement(doc, "TransportReason").text = "" #VERIFICAR DATO
                              ET.SubElement(doc, "GoodsAppearance").text = "" #VACIO
                              ET.SubElement(doc, "NumOfPieces").text = "" #VACIO
                              ET.SubElement(doc, "TransportDateTime").text = "" #VACIO
                              ET.SubElement(doc, "ShipmentTerms").text = "" #VERIGICAR TIPO DE DATO
                              ET.SubElement(doc, "TransportedWeight").text = "" #VACIO
                              ET.SubElement(doc, "TrackingNumber").text = "" #VACIO
                              ET.SubElement(doc, "InternalComment").text = "" #VACIO
                              ET.SubElement(doc, "CustomField1").text = "" #VACIO
                              ET.SubElement(doc, "CustomField2").text = "" #VACIO
                              ET.SubElement(doc, "CustomField3").text = "" #VACIO
                              ET.SubElement(doc, "CustomField4").text = "" #VACIO
                              ET.SubElement(doc, "FootNotes").text = note #NOTA DE LA ORDEN
                              ET.SubElement(doc, "ExpectedConclusionDate").text = "" #VACIO
                              ET.SubElement(doc, "SalesAgent").text = str(details_user_select[0][1]) #VENDEDOR
                              
                              rows = ET.SubElement(doc,"Rows")
                              
                              ###### ABRIR LA BBDD PARA EXTRAER LOS PRODUCTOS QUE CONTIENEN ESTE NUMERO DE ORDEN ##########
                              cur.execute("SELECT * FROM order_details WHERE id_order = %s", (id_order))
                              details_products_select = cur.fetchall()
                              
                              for x in details_products_select:
                                    name_product_select = x[7]
                                    
                                    cur.execute("SELECT * FROM products WHERE name = %s", name_product_select)
                                    if (product_act:= cur.fetchone()) is not None: 
                                          codice = product_act[1] 
                                          
                                          #### DATOS DE LOS DETALLES DE LA ORDEN #####
                                          row = ET.SubElement(rows, "Row")
                                          ET.SubElement(row, "Code").text = str(codice) #Codigo producto
                                          ET.SubElement(row, "Description").text = str(name_product_select) #descricion del producto            
                                          ET.SubElement(row, "Qty").text = str(qta_details) #Cantidad de producto agregados
                                          ET.SubElement(row, "Um").text = "pz"#tipo de empaque pz, box, kg etc
                                          ET.SubElement(row, "Lot").text = " " #VACIO
                                          ET.SubElement(row, "Price").text = str(price_unit_details) #Precio unitario
                                          ET.SubElement(row, "Discounts").text = str(id_discount_details)#Descuento
                                          ET.SubElement(row, "VatCode", Perc=str(iva_details), Class="Imponibile", Description="Imponibile " + str(iva_details)).text = str(iva_details) #Valor de Impuestos
                                          ET.SubElement(row, "Total").text = str(price_unit_details * qta_details) #Total del producto producto x cantidad
                                          ET.SubElement(row, "Stock").text = "true"#Disponibilidad TRUE O FALSE
                                          ET.SubElement(row, "CommissionPerc").text = str(details_user_select[0][5])#Comision vendedor
                                          ET.SubElement(row, "Notes").text = " "#NOTAS DEL PRODUCTO
                  filexml = ET.ElementTree(root)
                  filexml.write(os.path.join(os.getcwd(), "src", "static", "docs", "documenti.DefXml"))
                  
                  flash(f"{cont_orders} ordini sono stati esportati", "success")
                  return redirect(url_for("home"))   
            
            
