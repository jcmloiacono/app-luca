from app import app
#from flask_login import LoginManager


HOST = "localhost"
PORT = 4000
DEBUG = True


if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)
    